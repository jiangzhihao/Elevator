package com.jason.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * @author Jason_Jiang
 *
 */
public class Elevator implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final Integer maxLoadNumber = 12; //最大载人数
	
	public static final Integer intervalTime = 1; //运行一层楼的时间（单位 s）
	
	private int direction = 1; //运行方向（1：上 ； 0：下）
	
	private int level = 1; //当前所属楼层
		
	private Integer loadNumber = 0; //当前载人数

	private Set<Inhabitant> inhabitants; //当前乘客
	
	
}
